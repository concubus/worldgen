#+/bin/bash

terraform init
ssh-keygen -t rsa -f ./terraform
terraform apply --auto-approve
ssh -i terraform -l ec2-user -o StrictHostKeyChecking=no `cat ip`
rm ip
terraform destroy --auto-approve
